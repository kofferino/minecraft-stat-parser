#!/usr/bin/python

import os
import json
import sys

SAVE_DIR = "/saves"

minutesPlayed = 0


saves = [f.path for f in os.scandir(str(sys.argv[1]) + SAVE_DIR) if f.is_dir()]

for save in saves:
    statFiles = [f for f in os.scandir(save + "/stats") if f.name.endswith(".json")]
    minutesPlayed += int(json.load(open(statFiles[0].path, "r"))["stat.playOneMinute"]) / 1000


print("Minutes played (All worlds): " + str(int(round(minutesPlayed))))
